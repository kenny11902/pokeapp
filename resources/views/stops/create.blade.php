@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Stop</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('stops.store') }}"
                        aria-label="Create Stop">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label
                                text-md-right">Stop name</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                    class="form-control{{ $errors->has('name') ?
                                ' is-invalid' : '' }}" name="name"
                                value="{{$stop->name }}" required autofocus>

                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br>
                            <label for="name" class="col-md-12 col-form-label
                                text-md-left">Stop gps format:
                                52.87090420558121&6.001826226711273</label>
                            <b for="name" class="col-md-12 col-form-label
                                text-md-left">Stop gps gen :
                                <a href="https://www.openstreetmap.org">https://www.openstreetmap.org</a></label>
                            <div class="col-md-12">
                                <input id="gps" type="text"
                                    class="form-control" name="gps"
                                    value="{{$stop->gps }}" autofocus>


                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="town">Town</label>
                            <select class="form-control" id="town"
                                name="towns_id">
                                @foreach($towns as $town)
                                <option value="{{$town->id}}">{{$town->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

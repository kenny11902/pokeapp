@extends('layouts.main')
@section('content')
<div class="row title">
    <h1>Poke stops</h1>
</div>
<!-- <div class="row searchbar" style="padding:15px;">
    <div class="col-md-3">
        <div class="form-group">
            <label for="town">Town</label>
            <select class="form-control" id="town">
                <option>Wolvega</option>
                <option>Heerenveen</option>
                <option>De Blesse</option>
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="mission">Mission</label>
            <select class="form-control" id="mission">
                <option>Catch 3</option>
                <option>Catch 4</option>
                <option>Catch 5</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="reward">Reward</label>
            <select class="form-control" id="reward">
                <option>Cubone</option>
                <option>Ghastly</option>
                <option>Mewto</option>
            </select>
        </div>
    </div>
</div> -->
<!-- <div class="row filters-on">
    <div class="col-md-12">
        <span class="badge badge-primary">Cubone</span>
        <span class="badge badge-primary">Wolvega</span>
    </div>
</div>
<hr> -->
<div class="row">
    @guest
    @else
    <div class="col-lg-1">
        @if(\Auth::user()->group == 42)
        <a href="{{ route('stops.create') }}" class="btn btn-primary">
            Add
        </a>
        @endif
    </div>
    @endguest
</div>
@if(isset($stops))
<br>
<div class="row">

    <div class="container">
        <form action="{{ route('stops.search') }}" method="POST" role="search">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" class="form-control" name="q"
                    placeholder="Search stops" value="<?=isset($q)?$q:''?>">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        <i class="fa
                            fa-search bigfonts" aria-hidden="true"></i>
                    </button>
                </span>
            </div>
        </form>
    </div>
</div>
<br>
<div class="row">

    <div class="col-12">
        {!! $stops->render() !!}
    </div>
</div>
<div class="row">

    @foreach ( $stops as $stop)

    <div id="content" class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
        @include('partial.stop_card',
        [$title=$stop->name,
        $id=$stop->id,
        $town=$stop->Town->name,
        $task=$stop->Task!=null?$stop->Task->description:'none',
        $stopUpdatedTime=$stop->updated_at,
        $stopUpdatedToday=$stop->updated_at->isToday(),
        $gps=$stop->gps,
        $reward=$stop->Reward!=null?$stop->Reward->description:'none',
        ])
        @guest
        @else
        @if(\Auth::user()->group == 42)
        <div class="row">
            <div class="col-6">
                <a href="{{ route('stops.edit',$stop->id)}}" class="btn
                    btn-primary">Edit</a>
            </div>
            <div class="col-6">
                <form action="{{ route('stops.destroy',[$stop->id])}}"
                    method="post" style="float:right;">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </div>
        </div>


        @endif
        @endguest
    </div>
    @endforeach

</div>
<br>
<div class="row">

    <div class="col-12">
        {!! $stops->render() !!}
    </div>
</div>
@endif
@stop

@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Stop</div>

                <div class="card-body">
                    <form method="POST" action="{{
                        route('stop.store.task') }}"
                        aria-label="Set stop task">
                        @csrf
                        <input type="hidden" name="stop_id" value="{{$stop->id}}">
                        <div class="form-group row">
                            <label for="task">Task</label>
                            <select class="form-control" id="task"
                                name="tasks_id">
                                @foreach($tasks as $task)
                                <option value="{{$task->id}}">{{$task->description}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="reward">Reward</label>
                            <select class="form-control" id="reward"
                                name="rewards_id">
                                @foreach($rewards as $reward)
                                <option value="{{$reward->id}}">{{$reward->description}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

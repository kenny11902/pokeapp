<div class="card" style="margin-top:15px;margin-bottom:15px;">
    <!-- <img src="{{ asset('/images/notfound.png') }}" class="card-img-top" alt="not
        found image"> -->
    <?php
        $pieces='';
        if($gps!=''){
        $pieces= explode('&' , $gps);
        }

        if(isset($pieces[1])){


        $lat= $pieces[0];
        $lon= $pieces[1];
        $latHigh= $lat + 0.00071399195;
        $lonHigh= $lon + 0.00223964452;
        $latLow= $lat - 0.00071399195;
        $lonLow= $lon - 0.00223964452;
        $latRound=round($lat,5);
        $lonRound=round($lon,5);
        //bbox= min Longitude , min Latitude , max Longitude , max Latitude

        ?>
        <iframe height="250" frameborder="0" scrolling="no"
            marginheight="0" marginwidth="0"
            src="https://www.openstreetmap.org/export/embed.html?bbox=<?=$lonLow?>%2C<?=$latLow?>%2C<?=$lonHigh?>%2C<?=$latHigh?>&amp;layer=mapnik&amp;marker=<?=$lat?>%2C<?=$lon?>"
                                style="border: 1px solid black"></iframe><br/><small><a
                                        href="https://www.openstreetmap.org/?mlat=<?=$latRound?>&amp;mlon=<?=$lonRound?>#map=19/<?=$latRound?>/<?=$lonRound?>">Grotere
                                                    kaart bekijken</a></small>
                                            <?php }else{?>
                                                <img src="{{
                                                    asset('/images/notfound.png')
                                                    }}" class="card-img-top"
                                                    alt="not
                                                    found image">
                                                <?php }?>
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{
                                                            $title
                                                            }}</h5>
                                                        <p class="card-text">{{
                                                            $town }}</p>
                                                        @if($task!='none')
                                                        <div class="row">
                                                            <div
                                                                class="col-md-12">Mission</div>
                                                            @if($stopUpdatedToday)
                                                            <div
                                                                class="col-md-12"><span
                                                                    class="badge
                                                                    badge-info">{{
                                                                    $stopUpdatedTime->format('d-m-Y')
                                                                    }}</span></div>
                                                            @else
                                                            <div
                                                                class="col-md-12"><span
                                                                    class="badge
                                                                    badge-danger">{{
                                                                    $stopUpdatedTime->format('d-m-Y')
                                                                    }}</span>
                                                            </div>
                                                            @endif

                                                            <div
                                                                class="col-md-12"><b><p>{{
                                                                        $task }}</p></b></div>
                                                        </div>
                                                        <div class="row">
                                                            <div
                                                                class="col-md-12"><p>{{
                                                                    $reward }}</p></div>
                                                        </div>
                                                        @endif
                                                        <div class="row">
                                                            <div
                                                                class="col-xs-12
                                                                col-md-6">

                                                                <a href="{{
                                                                    route('stops.updatetask',$id)
                                                                    }}"
                                                                    class="btn
                                                                    btn-primary">Update
                                                                    Mission</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

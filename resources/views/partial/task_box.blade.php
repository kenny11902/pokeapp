<div class="card" style="margin-top:15px;margin-bottom:15px;">
    <div class="card-body">
        <h5 class="card-title">{{ $title }}</h5>
        <p class="card-text">{{ $description }}</p>
    </div>
</div>

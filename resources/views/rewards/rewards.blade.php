@extends('layouts.main')
@section('content')
<div class="row title">
    <h1>Poke rewards</h1>
</div>

<div class="row">
    @guest
    @else
    <div class="col-lg-1">
        @if(\Auth::user()->group == 42)
        <a href="{{ route('rewards.create') }}" class="btn btn-primary">
            Add
        </a>
        @endif
    </div>
    @endguest
</div>
<div class="sizedbox" style="padding: 10px;"></div>
<div class="row">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>
                <th scope="col">Active</th>
                <th scope="col">Created_at</th>
                <th scope="col">Updated_at</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $rewards as $reward)
            <tr>
                <th scope="row">{{ $reward->id}}</th>
                <td>{{ $reward->title}}</td>
                <td>{{ $reward->description}}</td>
                <td>{{ $reward->status}}</td>
                <td>{{ $reward->active}}</td>
                <td>{{ $reward->created_at}}</td>
                <td>{{ $reward->updated_at}}</td>
                <td>
                    <form action="{{ route('rewards.destroy',[$reward->id])}}"
                        method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>


</div>
@stop

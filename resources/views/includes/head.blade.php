<meta charset="utf-8">
<meta name="description" content="poke stops and raids">
<meta name="author" content="Kenny de Jong">

<title>Pokeapp</title>

<!-- load bootstrap from a cdn must be local at some time-->

<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"
    />
<link href="{{ asset('/font-awesome/css/font-awesome.min.css') }}"
    rel="stylesheet"
    type="text/css" />
<link rel="stylesheet" href="{{ asset('/css/main.css') }}">

<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Pokeapp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse"
        data-target="#navbarCollapse" aria-controls="navbarCollapse"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ request()->is('home') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('logged_in_home') }}">Home
                    <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ request()->is('stops*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('stops.index') }}">Stops</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Gyms</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Raid Groups</a>
            </li>
            @auth
            <li class="nav-item {{ request()->is('tasks*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('tasks.index') }}">Tasks</a>
            </li>
            <li class="nav-item {{ request()->is('rewards*') ? 'active' : ''
                }}">
                <a class="nav-link" href="{{ route('rewards.index') }}">Rewards</a>
            </li>
            @endauth
        </ul>

        @if (Route::has('login'))
        <ul class="nav navbar-nav pull-right">
            @auth
            <li class="nav-item">
                <a class="nav-link" href="{{
                    route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{
                    route('logout') }}" method="POST"
                    style="display: none;">
                    @csrf
                </form>
            </li>
            @else
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>

            @if (Route::has('register'))
            <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">Register</a>
            </li>
            @endif
            @endauth
        </ul>
        @endif
        {{-- <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search"
                aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form> --}}
    </div>
</nav>

@extends('layouts.main')
@section('content')
<div class="row title">
    <h1>Poke tasks</h1>
</div>

<div class="row">
    @guest
    @else
    <div class="col-lg-1">
        @if(\Auth::user()->group == 42)
        <a href="{{ route('tasks.create') }}" class="btn btn-primary">
            Add
        </a>
        @endif
    </div>
    @endguest
</div>
<div class="sizedbox" style="padding: 10px;"></div>
<div class="row">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>
                <th scope="col">Active</th>
                <th scope="col">Created_at</th>
                <th scope="col">Updated_at</th>
                <th scope="col">Delete</th>
                <th scope="col">Edit</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $tasks as $task)
            <tr>
                <th scope="row">{{ $task->id}}</th>
                <td>{{ $task->title}}</td>
                <td>{{ $task->description}}</td>
                <td>{{ $task->status}}</td>
                <td>{{ $task->active}}</td>
                <td>{{ $task->created_at}}</td>
                <td>{{ $task->updated_at}}</td>
                <td>
                    <form action="{{ route('tasks.destroy',[$task->id])}}"
                        method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
                <td>
                    <a href="{{ route('tasks.edit',$task->id)}}" class="btn
                        btn-primary">Edit</a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>


</div>
@stop

@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Task</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tasks.store') }}"
                        aria-label="Create Task">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label
                                text-md-right">Task title</label>

                            <div class="col-md-6">
                                <input id="title" type="text"
                                    class="form-control{{ $errors->has('title')
                                ?
                                ' is-invalid' : '' }}" name="title" value="{{
                                old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4
                                col-form-label
                                text-md-right">Task description</label>

                            <div class="col-md-6">
                                <input id="description" type="text"
                                    class="form-control{{ $errors->has('description')
                                ?
                                ' is-invalid' : '' }}" name="description"
                                value="{{
                                old('description') }}" required autofocus>

                                @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

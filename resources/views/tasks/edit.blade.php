@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Task</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tasks.update', $task->id)
                        }}"
                        aria-label="Update task">
                        @method('PATCH')
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label
                                text-md-right">Task title</label>

                            <div class="col-md-6">
                                <input id="title" type="text"
                                    class="form-control{{ $errors->has('title')
                                ?
                                ' is-invalid' : '' }}" name="title"
                                value="{{$task->title }}" required autofocus>
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4
                                col-form-label
                                text-md-right">Task description</label>

                            <div class="col-md-6">
                                <input id="description" type="text"
                                    class="form-control{{ $errors->has('description')
                                ?
                                ' is-invalid' : '' }}" name="description"
                                value="{{$task->description }}" required
                                autofocus>

                                @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">Add Reward</div>


                <div class="card-body">
                    <form method="POST" action="{{ route('tasks.update', $task->id)
                        }}"
                        aria-label="Update task">
                        @method('PATCH')
                        @csrf
                        <div class="form-group row">
                            <label for="rewards_id" class="col-md-4
                                col-form-label
                                text-md-right">Reward</label>
                            <div class="col-md-6">
                                <select class="form-control" id="reward"
                                    name="rewards_id">
                                    @foreach($rewards as $reward)
                                    <option value="{{$reward->id}}">{{$reward->title}}
                                        : {{$reward->description}}</option>
                                    @endforeach

                                </select>
                            </div>

                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    @if($task->rewards->count()!=0)
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">Connected Reward</div>


                <div class="card-body">
                    <form method="POST" action="{{ route('tasks.update',
                        $task->id)
                        }}"
                        aria-label="Update task">
                        @method('PATCH')
                        @csrf
                        <div class="form-group row">
                            <label for="rewards_id" class="col-md-4
                                col-form-label
                                text-md-right">Rewards </label>
                            <div class="col-md-6">
                                <select class="form-control" id="reward"
                                    name="connected_rewards_id">
                                    @foreach($task->rewards as $reward)
                                    <option value="{{$reward->id}}">{{$reward->title}}
                                        : {{$reward->description}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn
                                    btn-danger">
                                    remove
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

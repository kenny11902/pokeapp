<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stop extends Model
{
    public function Town()
    {
        return $this->hasOne('App\Town', 'id', 'towns_id');
    }
    public function Task()
    {
        return $this->hasOne('App\Task', 'id', 'tasks_id');
    }
    public function Reward()
    {
        return $this->hasOne('App\Reward', 'id', 'rewards_id');
    }
}
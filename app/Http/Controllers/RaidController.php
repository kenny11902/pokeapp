<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Raid;
use App\Http\Resources\Raid as RaidResource;
use View;


class RaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get raids
        $raids = Raid::paginate(15);
        // Return collection of raids as a resource
        return RaidResource::collection($raids);

    }
    public function web_index()
    {
        $raids = Raid::paginate(15);
        return View::make('raids.raids', compact('raids'));
    }
    public function web_create()
    {
        $raid = new Raid();
        return View::make('raids.create', compact('raid'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $raid = $request->isMethod('put') ? Raid::findOrFail($request->raid_id) : new Raid;
        $raid->id = $request->input('raid_id');
        $raid->start = $request->input('start');
        $raid->raid_bosses_id = $request->input('raid_bosses_id');
        $raid->stops_id = $request->input('stops_id');
        if ($raid->save()) {
            return new RaidResource($raid);
        }
    }

    public function show($id)
    {
        // Get raid
        $raid = Raid::findOrFail($id);
        // Return single raid as a resource
        return new RaidResource($raid);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get raid
        $raid = Raid::findOrFail($id);
        if ($raid->delete()) {
            return new RaidResource($raid);
        }
    }
}
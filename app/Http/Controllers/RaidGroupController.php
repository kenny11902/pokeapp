<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Raid_group;
use App\Http\Resources\Raid_group as Raid_groupResource;

class RaidGroupController extends Controller
{
    public function index()
    {

        // Get raidGroups
        $raidGroups = Raid_group::paginate(15);
        // $raidGroups = Raid_group::with('Raid','Raid.Stop','Raid.Stop.Town')->with('Group_members')->paginate(15);
        // Return collection of raidGroups as a resource
        return Raid_GroupResource::collection($raidGroups);

    }
    public function web_index()
    {
        $raidGroups = Raid_group::paginate(15);
        // $raidGroups = Raid_group::with('Raid','Raid.Stop','Raid.Stop.Town')->with('Group_members')->paginate(15);
        return View::make('raidGroups.raidGroups', compact('raidGroups'));
    }
    public function web_create()
    {
        $raidGroup = new raidGroup();
        return View::make('raidGroups.create', compact('raidGroup'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $raidGroup = $request->isMethod('put') ? Raid_group::findOrFail($request->raidGroup_id) : new raidGroup;
        $raidGroup->id = $request->input('raidGroup_id');
        $raidGroup->name = $request->input('name');
        $raidGroup->start_time = $request->input('start_time');
        if ($raidGroup->save()) {
            return new Raid_GroupResource($raidGroup);
        }
    }

    public function show($id)
    {
        // Get raidGroup
        $raidGroup = Raid_group::findOrFail($id);
        // Return single raidGroup as a resource
        return new Raid_GroupResource($raidGroup);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get raidGroup
        $raidGroup = Raid_group::findOrFail($id);
        if ($raidGroup->delete()) {
            return new Raid_GroupResource($raidGroup);
        }
    }
}
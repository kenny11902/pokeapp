<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group_member;
use App\Http\Resources\Group_member as Group_memberResource;
use View;

class GroupMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get Group_memberes
        $Group_members = Group_member::paginate(15);
        // Return collection of Group_memberes as a resource
        return Group_memberResource::collection($Group_members);

    }
    public function web_index()
    {
        $Group_members = Group_member::paginate(15);
        return View::make('groupMember.groupMembers', compact('Group_members'));
    }
    public function web_create()
    {
        $Group_member = new Group_member();
        return View::make('groupMember.create', compact('Group_member'));
    }

    public function store(Request $request)
    {
        $Group_member = $request->isMethod('put') ? Group_member::findOrFail($request->group_member_id) : new Group_member;
        $Group_member->id = $request->input('group_member_id');
        $Group_member->raid_groups_id = $request->input('raid_groups_id');
        $Group_member->users_id = $request->input('users_id');
        $Group_member->amount = $request->input('amount');
        $Group_member->description = $request->input('description') ? $request->input('description') : '';

        if ($Group_member->save()) {
            return new Group_memberResource($Group_member);
        }
    }

    public function show($id)
    {
        // Get Group_member
        $Group_member = Group_member::findOrFail($id);
        // Return single raid as a resource
        return new Group_memberResource($Group_member);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get Group_member
        $Group_member = Group_member::findOrFail($id);
        if ($Group_member->delete()) {
            return new Group_memberResource($Group_member);
        }
    }
}
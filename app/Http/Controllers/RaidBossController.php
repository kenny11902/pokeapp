<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Raid_boss;
use App\Http\Resources\Raid_boss as Raid_bossResource;
use View;

class RaidBossController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get raidbosses
        $raidbossbosses = Raid_boss::paginate(15);
        // Return collection of raidbosses as a resource
        return Raid_bossResource::collection($raidbossbosses);

    }
    public function web_index()
    {
        $raidbossbosses = Raid_boss::paginate(15);
        return View::make('raidboss.raidbosses', compact('raidbossbosses'));
    }
    public function web_create()
    {
        $raidboss = new Raid_boss();
        return View::make('raidboss.create', compact('raidboss'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $raidboss = $request->isMethod('put') ? Raid_boss::findOrFail($request->raidboss_id) : new Raid_boss;
        $raidboss->id = $request->input('raidboss_id');
        $raidboss->name = $request->input('name');
        $raidboss->rating = $request->input('rating');
        $raidboss->img = $request->input('img');
        $raidboss->active = $request->input('active') ? $request->input('active') : 0;
        $raidboss->status = $request->input('status') ? $request->input('status') : 'requested';

        if ($raidboss->save()) {
            return new Raid_bossResource($raidboss);
        }
    }

    public function show($id)
    {
        // Get raidboss
        $raidboss = Raid_boss::findOrFail($id);
        // Return single raid as a resource
        return new Raid_bossResource($raidboss);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get raidboss
        $raidboss = Raid_boss::findOrFail($id);
        if ($raidboss->delete()) {
            return new Raid_bossResource($raidboss);
        }
    }
}
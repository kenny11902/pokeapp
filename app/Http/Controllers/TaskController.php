<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Task;
use App\Reward;
use App\Http\Resources\Task as TaskResource;
use View;
use function GuzzleHttp\Promise\all;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     // Get tasks
    //     $tasks = Task::all();
    //     // Return collection of tasks as a resource
    //     return TaskResource::collection($tasks);
    // }
    public function index()
    {
        $tasks = Task::all();
        return View::make('tasks.tasks', compact('tasks'));
    }
    public function create()
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $task = new Task();
        return View::make('tasks.create', compact('task'));
    }
    public function edit($id)
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $rewards = Reward::all();
        $task = Task::with('rewards')->find($id);
        return View::make('tasks.edit', compact('task', 'rewards'));
    }
    public function update(Request $request, $id)
    {
        if (\Auth::user()->group != 42 && !Task::findOrFail($id)) {
            return redirect('home');
        }


        if ($request->input('rewards_id')) {
            $task = Task::findOrFail($id);
            $task->rewards()->attach($request->input('rewards_id'));
            return redirect()->route('tasks.edit', $id);
        } else
        if ($request->input('connected_rewards_id')) {
            $task = Task::findOrFail($id);
            $task->rewards()->detach($request->input('connected_rewards_id'));
            return redirect()->route('tasks.edit', $id);
        } else {
            $task = Task::findOrFail($id);
            $task->title = $request->input('title');
            $task->description = $request->input('description');
            if ($task->save()) {
                return redirect('tasks');
            }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     if (\Auth::user()->group != 42) {
    //         return redirect('home');
    //     }
    //     $task = $request->isMethod('put') ? Task::findOrFail($request->task_id) : new Task;
    //     $task->id = $request->input('task_id');
    //     $task->title = $request->input('title');
    //     $task->description = $request->input('description');
    //     $task->active = $request->input('active') ? $request->input('active') : 0;
    //     $task->status = $request->input('status') ? $request->input('status') : 'requested';
    //     if ($task->save()) {
    //         return new TaskResource($task);
    //     }
    // }
    public function store(Request $request)
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $task = $request->isMethod('put') ? Task::findOrFail($request->task_id) : new Task;
        $task->id = $request->input('task_id');
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->active = $request->input('active') ? $request->input('active') : 0;
        $task->status = $request->input('status') ? $request->input('status') : 'requested';
        if ($task->save()) {
            return redirect('tasks');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     // Get task
    //     $task = Task::findOrFail($id);
    //     // Return single task as a resource
    //     return new TaskResource($task);
    // }

    public function destroy($id)
    {

        if (\Auth::user()->group != 42) {
            return redirect('home');
        }

        $task = Task::findOrFail($id);
        if ($task->delete()) {
            return redirect('tasks');
        }
    }
}
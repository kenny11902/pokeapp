<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Town;
use App\Http\Resources\Town as TownResource;

class TownController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get towns
        $towns = Town::paginate(15);
        // Return collection of towns as a resource
        return TownResource::collection($towns);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $town = $request->isMethod('put') ? Town::findOrFail($request->town_id) : new Town;
        $town->id = $request->input('town_id');
        $town->name = $request->input('name');
        $town->gps = '';
        $town->country_code = $request->input('country_code');
        $town->status = $request->input('status') ? $request->input('status') : 'requested';
        if ($town->save()) {
            return new TownResource($town);
        }
    }

    public function show($id)
    {
        // Get town
        $town = Town::findOrFail($id);
        // Return single town as a resource
        return new TownResource($town);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get town
        $town = Town::findOrFail($id);
        if ($town->delete()) {
            return new TownResource($town);
        }
    }
}
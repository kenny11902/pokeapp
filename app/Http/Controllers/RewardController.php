<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Reward;
use View;
use App\Http\Resources\Reward as RewardResource;
use function GuzzleHttp\Promise\all;

class RewardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get rewards
        $rewards = Reward::all();
        // Return collection of rewards as a resource
        return View::make('rewards.rewards', compact('rewards'));
    }

    public function create()
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $rewards = Reward::all();
        return View::make('rewards.create', compact('rewards'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $reward = $request->isMethod('put') ? Reward::findOrFail($request->reward_id) : new Reward;
        $reward->id = $request->input('reward_id');
        $reward->title = $request->input('title');
        $reward->description = $request->input('description');
        $reward->active = $request->input('active') ? $request->input('active') : 0;
        $reward->status = $request->input('status') ? $request->input('status') : 'requested';
        if ($reward->save()) {
            return redirect('rewards');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     // Get reward
    //     $reward = Reward::findOrFail($id);
    //     // Return single reward as a resource
    //     return new RewardResource($reward);
    // }

    public function destroy($id)
    {
        // Get reward
        $reward = Reward::findOrFail($id);
        if ($reward->delete()) {
            return redirect('rewards');
        }
    }
}
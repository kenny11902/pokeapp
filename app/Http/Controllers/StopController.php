<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Stop;
use App\Town;
use App\Task;
use App\Reward;
use App\Http\Resources\Stop as StopResource;
use View;
use function GuzzleHttp\Promise\all;

class StopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     // Get stops
    //     $stops = Stop::with('Town')->with('Task')->with('Reward')->paginate(15);
    //     // Return collection of stops as a resource
    //     return StopResource::collection($stops);

    // }
    public function index()
    {

        $stops = Stop::orderBy('name')->with('Town')->with('Task')->with('Reward')->paginate(6);
        return View::make('stops.stops', compact('stops'));
    }
    public function search()
    {
        $q = Input::get('q');

        $stops = Stop::where('name', 'LIKE', '%' . $q . '%')->orderBy('name')->with('Town')->with('Task')->with('Reward')->paginate(6)->setPath('');

        $pagination = $stops->appends(array(
            'q' => Input::get('q')
        ));
        return View::make('stops.stops', compact('stops', 'pagination', 'q'));
    }
    public function update_task($id)
    {
        $stop = Stop::with('Task')->with('Reward')->find($id);
        $tasks = Task::all();
        $rewards = Reward::all();

        // return $stop ;
        return View::make('stops.task', compact('stop', 'rewards', 'tasks'));
    }
    public function create()
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $towns = Town::all();
        $stop = new Stop();
        return View::make('stops.create', compact('stop', 'towns'));
    }
    public function edit($id)
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        $towns = Town::all();
        $stop = Stop::find($id);
        return View::make('stops.edit', compact('stop', 'towns'));
    }
    public function add_task_reward($id)
    {
        $stop = Stop::find($id);
        dd($stop);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     if (\Auth::user()->group != 42) {
    //         return redirect('home');
    //     }
    //     $stop = $request->isMethod('put') ? Stop::findOrFail($request->stop_id) : new Stop;
    //     $stop->id = $request->input('stop_id');
    //     $stop->name = $request->input('name');
    //     $stop->gym = $request->input('gym') ? $request->input('gym') : 0;
    //     $stop->towns_id = $request->input('towns_id');
    //     $stop->tasks_id = $request->input('tasks_id') ? $request->input('tasks_id') : 0;
    //     $stop->rewards_id = $request->input('rewards_id') ? $request->input('rewards_id') : 0;
    //     $stop->gps = '';
    //     $stop->img = '';
    //     $stop->status = $request->input('status') ? $request->input('status') : 'requested';
    //     if ($stop->save()) {
    //         return new StopResource($stop);
    //     }
    // }
    public function store(Request $request)
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        die('store');
        $stop = $request->isMethod('put') ? Stop::findOrFail($request->stop_id) : new Stop;
        $stop->id = $request->input('stop_id');
        $stop->name = $request->input('name');
        $stop->gym = $request->input('gym') ? $request->input('gym') : 0;
        $stop->towns_id = $request->input('towns_id');
        $stop->tasks_id = $request->input('tasks_id') ? $request->input('tasks_id') : 0;
        $stop->rewards_id = $request->input('rewards_id') ? $request->input('rewards_id') : 0;
        $stop->gps = $request->input('gps') ? $request->input('gps') : '';
        $stop->img = '';
        $stop->status = $request->input('status') ? $request->input('status') : 'requested';
        if ($stop->save()) {
            return redirect('stops');
        }
    }
    public function update(Request $request, $id)
    {
        if (\Auth::user()->group != 42 && !Stop::findOrFail($id)) {
            return redirect('home');
        }

        $stop = Stop::findOrFail($id);
        $stop->name = $request->input('name');
        $stop->gym = $request->input('gym') ? $request->input('gym') : 0;
        $stop->towns_id = $request->input('towns_id');
        $stop->tasks_id = $request->input('tasks_id') ? $request->input('tasks_id') : 0;
        $stop->rewards_id = $request->input('rewards_id') ? $request->input('rewards_id') : 0;
        $stop->gps = $request->input('gps') ? $request->input('gps') : '';
        $stop->img = '';
        $stop->status = $request->input('status') ? $request->input('status') : 'requested';
        if ($stop->save()) {
            return redirect('stops');
        }
    }
    public function store_task(Request $request)
    {

        $stop = Stop::findOrFail($request->stop_id);
        $stop->tasks_id = $request->input('tasks_id') ? $request->input('tasks_id') : 0;
        $stop->rewards_id = $request->input('rewards_id') ? $request->input('rewards_id') : 0;

        if ($stop->save()) {
            return redirect('stops');
        } else {
            return redirect('stops');
        }
    }

    // public function show($id)
    // {
    //     // Get stop
    //     $stop = Stop::findOrFail($id);
    //     // Return single stop as a resource
    //     return new StopResource($stop);
    // }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Auth::user()->group != 42) {
            return redirect('home');
        }
        // Get stop
        $stop = Stop::findOrFail($id);
        if ($stop->delete()) {
            return redirect('stops');
        }
    }
}
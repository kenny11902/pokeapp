<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    public function rewards()
    {
        return $this->belongsToMany(
            'App\Reward',
            'tasks_rewards',
            'tasks_id',
            'rewards_id'
        );
    }
}
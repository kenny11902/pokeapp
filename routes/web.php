<?php

use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Route::get('/home', function () {

// });
Route::get('/', 'StopController@index')->name('logged_out_home');
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/home', 'StopController@index')->name('logged_in_home');
// Route::group(['middleware' => 'auth'], function () {
//     Route::post('stops', 'StopController@web_post')->name('stops_post');
// });
// stops
Route::resource(
    'stops',
    'StopController',
    ['only' => ['index', 'create', 'store', 'destroy', 'edit', 'update']]
);
Route::any('/stops/search', 'StopController@search')->name('stops.search');

Route::get('/stops/{id}/updatetask', 'StopController@update_task')->name('stops.updatetask');
Route::post('/stops/task/store', 'StopController@store_task')->name('stop.store.task');
//Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {
//
//});
// Route::get('/stops', 'StopController@web_index')->name('stops');
// Route::get('/stops/create', 'StopController@web_create')->name('create_stop');
// Route::post('/stops/store', 'StopController@web_store')->name('store_stop');
// tasks
Route::resource(
    'tasks',
    'TaskController',
    ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]
);
// Route::get('/tasks', 'TaskController@web_index')->name('tasks.index');
// Route::get('/tasks/create', 'TaskController@web_create')->name('tasks.create');
// Route::post('/tasks/store', 'TaskController@web_store')->name('tasks.store');
// Route::delete('/tasks/destroy/{id}', 'TaskController@destroy')->name('tasks.destroy');
// rewards
Route::resource(
    'rewards',
    'RewardController',
    ['only' => ['index', 'create', 'store', 'destroy']]
);
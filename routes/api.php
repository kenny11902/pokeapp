<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// // list stops
// Route::get('stops', 'StopController@index');
// // list single stop
// Route::get('stop/{id}', 'StopController@show');
// // create new stop
// Route::post('stops', 'StopController@store');
// // update stop
// Route::put('stops', 'StopController@store');
// // delete stop
// Route::delete('stops', 'StopController@destroy');

// // list towns
// Route::get('towns', 'TownController@index');
// // list single town
// Route::get('town/{id}', 'TownController@show');
// // create new town
// Route::post('towns', 'TownController@store');
// // update town
// Route::put('towns', 'TownController@store');
// // delete town
// Route::delete('towns', 'TownController@destroy');
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        id
        name
        img
        added time// replaced witht he basic created and updated
        gps location // saved as string. need to look at for future 
        gym ( bool )
        status (accepted,requested)
        external towns_id
         */
        Schema::create('stops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('img', 255);
            $table->string('gps', 255);
            $table->tinyInteger('gym');
            $table->enum('status', ['requested', 'approved']);
            $table->integer('towns_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stops');
    }
}
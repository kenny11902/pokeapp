<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStopsTable extends Migration
{
    public function up()
    {
        // stop / gym has only 1 reward and 1 mission. but must be reset every day.
        Schema::table('stops', function ($table) {
            $table->integer('rewards_id');
            $table->integer('tasks_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stops', function ($table) {
            $table->dropColumn('rewards_id');
            $table->dropColumn('tasks_id');
        });
    }
}
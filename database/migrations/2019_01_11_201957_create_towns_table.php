<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        id
        name
        gps // saved as string. need to look at for future 
        added time// replaced witht he basic created and updated
        country code
        status (approved, requested)
         */
        Schema::create('towns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('gps', 255);
            $table->string('country_code', 255);
            $table->enum('status', ['requested', 'approved']);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('towns');
    }
}
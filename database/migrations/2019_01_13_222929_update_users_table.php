<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        id v
        name v
        password v
        group
        email v
        in_game_name
        team
        token
        timestamps.
        token_time
        status ( active, deleted,)
         */
        Schema::table('users', function ($table) {
            $table->integer('group');
            $table->string('in_game_name');
            $table->string('team');
            $table->string('token');
            $table->boolean('deleted');
            $table->string('token_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('group');
            $table->dropColumn('in_game_name');
            $table->dropColumn('team');
            $table->dropColumn('token');
            $table->dropColumn('deleted');
            $table->dropColumn('token_time');
        });
    }
}